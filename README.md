# Images

![test](https://files.delta-wings.net/uploads/projects/sn/codewrapper/capture1.PNG)]

- Menu

![test](https://files.delta-wings.net/uploads/projects/sn/codewrapper/capture2.PNG)

- after wrote

![test](https://files.delta-wings.net/uploads/projects/sn/codewrapper/capture3.PNG)

- after clicked on "Update"

![test](https://files.delta-wings.net/uploads/projects/sn/codewrapper/capture4.PNG)

## what I used

- summernote 
    - bootstrap
    - popper.js
    - jquery
- gulp (for compilation and live testing)
    
# Support for highlight.js

![test](https://files.delta-wings.net/uploads/projects/sn/codewrapper/capture4.PNG)
